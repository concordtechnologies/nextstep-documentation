@ECHO OFF

SET SPHINXOPTS=-W -E
make.bat latex

# Must install MiKTex: https://miktex.org/
cd build/latex
pdflatex.exe NEXTSTEP.tex
