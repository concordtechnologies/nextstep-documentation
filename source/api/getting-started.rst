Getting Started
===============
All public APIs exposed by the NEXTSTEP use `Representational State Transfer (REST) architecture`_. API documentation
presented here is platform and language agnostic and described simply as its HTTP request or response components.

In this section, you will find tutorials demonstrating how to integrate with NEXTSTEP using various platforms and
programming languages.

.. _Representational State Transfer (REST) architecture: https://www.tutorialspoint.com/restful/restful_introduction.htm

.. toctree::
   :glob:

   tutorials/*

