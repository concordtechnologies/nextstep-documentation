Document Return: Retrieval by HTTP PULL
=======================================

Introduction
------------
The Document Retrieval API enables applications to query the NEXTSTEP platform for document status. As
the monitored documents are completed and published for retrieval the files associated with each document
(image, metadata, searchable PDF, etc.) are then downloaded by as second call to the API. After downloading
files for a particular document, the caller also has the option to disposition the document within the Workflow
Application by transitioning that document to an archived or deleted state, or purging the document completely.

The specific files that will be included in the delivery request are defined by the process configuration,
which should be be discovered, documented and implemented by the process owner or administrator prior to starting
the API integration.

Request Details
---------------
The lifecycle of requests for an application consuming the Document Retrieval APIs consists of two calls: List
Documents and Retrieve Document.

List Document
^^^^^^^^^^^^^
The list document request is made each time the application needs to query for available documents. The set of returned
documents will indicate the document processing or publishing status, as well as the queue and folder where the document
is currently stored.

The set of documents returned will be dependent on the authorization permissions applied to the authenticated user
calling the API.

.. list-table:: **Request Properties**
   :header-rows: 1
   :widths: 20, 25, 55

   * - Name
     - Location
     - Sample Value & Description

   * - **Method**
     - Request
     - *GET*

       The HTTP method/verb to use when calling this API.

   * - **Request URL**
     - Request
     - *https://nsapi.concord.net/api/retrieval*

       The retrieval endpoint used to list available documents.

   * - **X-Auth-User**
     - Header
     - *123456*

       The User ID representing the user executing the list API call.

   * - **X-Auth-ApiKey**
     - Header
     - *ab123456-1234-1234-12ab-1ab23c4de567*

       The API key corresponding to the X-AuthUser. Taken together the User ID and API Key are used
       to authenticate the user to the platform.

   * - **queueId**
     - Query (Optional)
     - *1234567abc12defg*

       The Queue Identifier filter option enables filtering of the returned documents to those items
       that are assigned to the specified workflow queue. When omitted documents in any queue will be
       returned.

       **The value here is a sample and should be replaced with a real value**

   * - **folderId**
     - Query (Optional)
     - *ab123456-1234-1234-12ab-1ab23c4de567*

       The Folder Identifier filter option enables filtering of the returned documents to those items
       that are assigned to the specified folder within a queue. When omitted documents in any folder
       will be returned.

       **The value here is a sample and should be replaced with a real value**

   * - **retrievalStatus**
     - Query (Optional)
     - *0*

       The Retrieval Status filter option enables filtering of the returned documents to those items
       that have the specified status assigned. When omitted documents with any retrieval status will
       be returned.

       Possible values include:
         - Pending or 0: Return documents with pending status
         - Completed or 1: Return documents with completed status

   * - **sort**
     - Query (Optional)
     - *0*

       The Sort option allows the caller to specify the sort direction of the returned documents. Documents
       are sorted by their Creation Date (**TODO: Needs confirmation). When omitted documents will be sorted
       in descending order by default.

       Possible values include:
         - Descending, DESC, or 0: Sort in descending order
         - Ascending, ASC, or 1: Sort in ascending order

   * - **take**
     - Query (Optional)
     - *50*

       The Take query option enables applications to implement paging of retrieved results using a skip/take
       model. The take option indicates the number of items to be returned, and the return data structure will
       indicate the total count of items available.

   * - **skip**
     - Query (Optional)
     - *100*

       The Skip query option enables applications to implement paging of retrieved results using a skip/take
       model. The skip option indicates the number of items to skip or bypass before selecting the item set to
       be returned, and the return data structure will indicate the total count of items available.

.. _list-example:

Example
*******
Below you will find an HTTP GET request and corresponding response for a sample document retrieval list
operation (in raw text format). The request is querying for pending documents in a specific queue only,
limited to 100 items returned, and not skipping any previously processed pages of items.

Sample Request::

  GET /api/retrieval?queueId=5c76a7035b53411a149de3d2&retrievalStatus=Pending&take=100&skip=0 HTTP/1.1
  Host: https://nsapi.concord.net/
  X-AuthUser: 123456
  X-AuthApiKey: ab123456-1234-1234-12ab-1ab23c4de567

..

Sample Response::

  Content-Type: application/json

  {
    "totalCount": 2,
    "documents": [
         {
            "id": "5c61181191acc818204667bb-7:5cecdee0a6cb577238656804",
            "queueId": "5c76a7035b53411a149de3d2",
            "folderId": "00000000-0000-0000-0000-000000000000",
            "files": [
                {
                    "id": "1bb0f308-7296-46a6-a8e2-f309856c8d47",
                    "format": "TIF",
                    "name": "document",
                    "fileName": "document.tif",
                    "version": 1,
                    "status": "Pending"
                },
                {
                    "id": "c9cfa81c-a1d8-40ee-aa2e-2cbf1f43d5b1",
                    "format": "JSON",
                    "name": "metadata",
                    "fileName": "metadata.json",
                    "version": 0,
                    "status": "Pending"
                }
            ],
            "status": "Pending",
            "sourceId": "1234-56789-01234",
            "altSourceId": "12345678901234"
         },
         {
            "id": "5c61181191acc818204667bb-7:5cecdee0a6cb577238656805",
            "queueId": "5c76a7035b53411a149de3d2",
            "folderId": "00000000-0000-0000-0000-000000000000",
            "files": [
                {
                    "id": "1bb0f308-7296-46a6-a8e2-f309856c8d47",
                    "format": "TIF",
                    "name": "document",
                    "fileName": "document.tif",
                    "version": 1,
                    "status": "Pending"
                },
                {
                    "id": "c9cfa81c-a1d8-40ee-aa2e-2cbf1f43d5b1",
                    "format": "JSON",
                    "name": "metadata",
                    "fileName": "metadata.json",
                    "version": 0,
                    "status": "Pending"
                }
            ],
            "status": "Pending",
            "sourceId": "1234-56789-01235",
            "altSourceId": "12345678901235"
         }
     ]
  }

..

.. list-table:: **Response Properties**
   :header-rows: 1
   :widths: 20, 25, 55

   * - Name
     - Location
     - Description

   * - **totalCount**
     - Response
     - The total number of documents matching the filter options.

   * - **documents**
     - Response
     - A JSON array containing the documents satisfying the query.

   * - **id**
     - Document Object
     - The unique identifier for the document. This value is used as a default value to retrieve the document and it's associated files when
       calling the document retrieval endpoint. (See below)

   * - **queueId**
     - Document Object
     - The unique identifier for the queue where the document is currently stored.

   * - **folderId**
     - Document Object
     - The unique identifier for the folder within the queue where the document is currently stored. This value
       may be an empty identifier (all zeroes) when the document is stored in the queue root folder.

   * - **files**
     - Document Object
     - A JSON array containing the files that have been published for retrieval for the current document.

   * - **id**
     - File Object
     - The unique identifier for the document file.

   * - **format**
     - File Object
     - The file format describing the file contents.

       Possible values include:

       - PDF
       - TIF
       - PNG
       - JPG
       - XML
       - TXT
       - JSON

   * - **name**
     - File Object
     - The logical file name (without extension) defined for this file by the Workflow Process Owner.

       API developers should work with the process owner to define logical names for files that will aid in
       locating and consuming the appropriate content from the API response.

       Note: For file types that produce multiple files, such as JPEG output of a multi-page document image,
       the logical file name will indicate the root portion of the file's complete name, with a page number and
       extension appended to form the complete file name.

       For JPEG output of a 3 page TIFF document image, where the logical filename selected is `document` the
       files returned would be:

       - document_1.jpg
       - document_2.jpg
       - document_3.jpg

   * - **fileName**
     - File Object
     - The complete file name (with any page number data and extension) defined for this file. This value is
       always an expansion of the logical name defined above.

   * - **version**
     - File Object
     - Indicates the document image file version this file represents.

       Possible values include:
        - 0: The original version of the document image
        - 1: The current version of the document image

       Note: This attribute only applies to the TIFF document image. For all other file types the version attribute
       will always be `0`.

   * - **status**
     - File Object
     - The file retrieval status, which indicates whether the current file has been downloaded or not.

       Possible values include:

       - Pending: The file has not been downloaded
       - Complete The file has been downloaded successfully

   * - **status**
     - Document Object
     - The document retrieval status, which indicates whether all files from the document has been downloaded or not.

       Possible values include:

       - Pending: The files have not been downloaded
       - Complete The files have been downloaded successfully

   * - **sourceId**
     - Document Object
     - A source message identifier that is generated by the originating system. This value can be used to retrieve the document 
       and it's associated files when calling the document retrieval endpoint. (See below)

   * - **altSourceId**
     - Document Object
     - The BR string varient of the sourceId to be able to query against FaxWS for status. This value can be used to retrieve the 
       document and it's associated files when calling the document retrieval endpoint. (See below)

Get Document
^^^^^^^^^^^^
The get document request should be made for each document to retrieve the files that have been published for that
document.

The set of files returned for each document will be dependent on the parameters passed to the API call.

.. list-table:: **Request Properties**
   :header-rows: 1
   :widths: 20, 25, 55

   * - Name
     - Location
     - Sample Value & Description

   * - **Method**
     - Request
     - *GET*

       The HTTP method/verb to use when calling this API.

   * - **Request URL**
     - Request
     - *https://nsapi.concord.net/api/retrieval/{documentId}*

       The endpoint used to retrieve published content for a single document. The document to retrieve is indicated
       by the `{documentId}` url element. The value passed here should correspond to the identifier returned in the
       list API response (as seen in our example above), such as: 
       
       - id: `5c61181191acc818204667bb-7:5cecdee0a6cb577238656804` 
       - sourceId: `1234-56789-01234`
       - altSourceId: `12345678901234`
       `id` in the response is the default value

   * - **X-AuthUser**
     - Header
     - *123456*

       The User ID representing the user executing the get API call.

   * - **X-AuthApiKey**
     - Header
     - *ab123456-1234-1234-12ab-1ab23c4de567*

       The API key corresponding to the X-AuthUser. Taken together the User ID and API Key are used
       to authenticate the user to the platform.

   * - **name**
     - Query (Optional)
     - The logical name of the file to retrieve. This is the file name without extension, as defined by the
       Workflow Process Owner. When specifying this value, only files matching the given name will be returned.

       API developers should work with the process owner to define logical names for files that will aid in
       locating and consuming the appropriate content from the API response.

       Note: For file types that produce multiple files, such as JPEG output of a multi-page document image,
       the logical file name will indicate the root portion of the file's complete name, with a page number and
       extension appended to form the complete file name. Specifying the logical name of `document` as shown below
       would return all 3 page of the file as independent files.

       For JPEG output of a 3 page TIFF document image, where the logical filename selected is `document` the
       files returned would be:

       - document_1.jpg
       - document_2.jpg
       - document_3.jpg

   * - **fileName**
     - Query (Optional)
     - The complete file name (with any page number data and extension) for the file to retrieve. See the description of
       file names described above in both the `name` query option and the documentation of the List method.

   * - **disposition**
     - Query (Optional)
     - Indicates the disposition that should be applied to the document after all files published for the document have
       been successfully downloaded.

       Possible values for disposition are as follows:

       - Retain: The document should be retained in its current location -- this is the default if no value is provided
       - Archive: The document should be moved to the Archive folder in the queue where it is currently stored
       - Delete: The document should be moved to the Deleted folder in the queue where it is currently stored
       - Purge: The document should be purged (permanently deleted)

       Note: Disposition will only be applied if all files within the document have a retrieval status of `Completed`.
       If there are any files pending retrieval, disposition will not be processed.

   * - **zip**
     - Query (Optional)
     - Indicates if the files for the document should be downloaded in a zip archive format. The default value is `false`.

       Note: When this is value is `true` the response will contain a single file that is a zipped archive of the requested
       files.

   * - **retrievalDownloadId**
     - Query (Optional)
     - Indicates what type of `id` will be passed in order to identify the document. The default value is `DocumentId`. Possible values are:

       - `DocumentId` or `0`
       - `SourceId` or `1`
       - `AltSourceId` or `2`
       
.. get-example:

Example
*******
Below you will find an HTTP GET request and corresponding response for a sample document retrieval get
operation (in raw text format). The request is querying for the files available for download for a specific
document, specifying that the document disposition should be `Retain`, and opting not to request the output
in a zip (archived) format.

The name and file name parameters are not specified, so all available files for the requested document will
be returned.

Please send appropriate dispostion value to move/purge the retrieved document,
        Retain 0
        Archive 1
        Delete 2
        Purge 3

Sample Request::

  GET /api/retrieval/5c61181191acc818204667bb-7:5cecdee0a6cb577238656804?disposition=0&zip=false HTTP/1.1
  Host: https://nsapi.concord.net/
  X-AuthUser: 123456
  X-AuthApiKey: ab123456-1234-1234-12ab-1ab23c4de567

..

Sample Response::

  Content-Type: multipart/form-data; boundary=----WebKitFormBoundaryWfPNVh4wuWBlyEyQ

  ------WebKitFormBoundaryWfPNVh4wuWBlyEyQ

  Content-Disposition: form-data; name="document"; filename="document.tif"
  Content-Type: image/tiff

  <binary file data>

  ------WebKitFormBoundaryWfPNVh4wuWBlyEyQ

  Content-Disposition: form-data; name="metadata"; filename="metadata.json"
  Content-Type: application/json

  {
    "sourceId": "123456789",
    "fields": [
      {
        "name": "patient_name",
        "value": "John Smith"
      },
      {
        "name": "patient_dob",
        "value": "02/20/1964"
      },
      {
        "name": "date_of_encounter",
        "value": "01/30/2018"
      },
      {
        "name": "mrn",
        "value": "142637A"
      }
    ]
  }

  ------WebKitFormBoundaryWfPNVh4wuWBlyEyQ

..

The content of the metadata file (if configured) in the multi-part form will depend on what has been published by the
process applied to the document.
