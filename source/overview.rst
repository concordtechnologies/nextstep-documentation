Overview
========
The NEXTSTEP API empowers developers to leverage the power of the platform's document
processing capabilities from any application or system.

This is achieved by providing a simple API that enables documents to be submitted to the
platform, processed, and then returned along with expanded, actionable metadata.

Let's begin by establishing a basic understanding of NEXTSTEP processing and activities.

Processes
---------
A NEXTSTEP Process defines a series of activities and configuration designed to
prepare and transform an incoming document into the central artifact in a business
process.

Processes are associated with documents during document intake, with selection of
the appropriate process determined by inspecting the metadata received with the incoming
document.

Once document intake is complete, process execution is managed by queueing the document
for processing in each scheduled activity in sequence. Once the final activity is executed
process execution is completed.

Activities
----------
NEXTSTEP activities each provide a distinct function designed to inspect, modify or augment
a document in some way. The set of available activities will continue to evolve with each
release.

.. list-table::
   :header-rows: 1
   :widths: 30, 70

   * - Activity
     - Description
   * - **Barcode Recognition**
     - Recognize and extract barcode values and metadata
   * - **Document Cleanup**
     - Perform common document imaging actions to improve image quality
   * - **Document Conversion**
     - Create alternate format representations of a document
   * - **Document Delivery**
     - Delivers image and metadata files to a configured external location
   * - **Document Separation**
     - Splits a document into multiple new documents
   * - **Page Rotation**
     - Applies page rotation to pages that are oriented incorrectly
   * - **Metadata Processing**
     - Maps metadata to custom fields in the Workflow application
   * - **Metadata Transform**
     - Transforms metadata into a text file format such as XML or JSON
   * - **OCR**
     - Recognizes text and related metadata in a document image
   * - **Signature Detection**
     - Inspects a location on a page to determine if a signature is present
   * - **Value Extraction**
     - Leverages AI to extract values from an unstructured document
